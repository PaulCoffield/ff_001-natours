# Natours

### (Frontend Folio - 0001)

#### Section 1 - Course intro

#### Section 2 - Building the header

- Background Image/s
- Clip-path
- CSS animation @keyframes, transition
- pseudo-elements & pseudo-classes

#### Section 3 - Rem, BEM & CSS Theory

- Converting PX to REM 🙌🏽
- CSS Architecture
- Using BEM Syntax

#### Section 4 - Sass, CLI & NPM

- Sass / SCSS
- Command Line Interface
- NPM

#### Section 5a - Sass & Grid setup

- More Sass - Nesting & Variables, 7-1 Architecture
- Custom Grid System with Floats

#### Section 5b - Building The About Section

- Thinking about components
- Utility classes
- Background-clip
- Transform multiple properties
- Outline & Outline-offset
- Styling when other elements are hovered

#### Section 5c - Building the Features Section

- Using an icon font
- 'Skewed' section design
- When to use direct child selector

#### Section 5d - Building the Tours Section

- Using perspective in CSS
- Backface-visibility
- Background blend modes
- When to use box-decoration-break

#### Section 5c - Building the Stories Section

- Text flow around images with shape-outside & float
- Applying a filter to images
- Using the Video element
- how and when to use object-fit
